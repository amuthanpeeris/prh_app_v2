﻿using System.Data.SqlClient;
using System.Threading.Tasks;

using PRH.Azure.Common.DataMapping.Common.Models;
using PRH.Azure.Common.DataMapping.SQL.Interfaces;

namespace PRH.Azure.Common.DataMapping.SQL
{
    /// <summary>
    /// Contains Database Operations
    /// </summary>
    public class ReferenceDataOperations: IReferenceDataOperations
    {
        /// <summary>
        /// Reference Data Lookup Operation
        /// </summary>
        /// <param name="referenceData">Object containing reference data</param>
        /// <param name="sqlConnString">Database connection string</param>
        /// <returns>Target Data (list)</returns>
        public string LookUpReferenceData(ReferenceData referenceData, string sqlConnString)
        {
            using var connection = new SqlConnection(sqlConnString);
            var command = new SqlCommand("[dbo].[LookUpReferenceData]", connection)
            {
                CommandType = System.Data.CommandType.StoredProcedure
            };
            command.Parameters.AddWithValue("@srcSysName", referenceData?.SourceSystem);
            command.Parameters.AddWithValue("@dstSysName", referenceData?.TargetSystem);
            command.Parameters.AddWithValue("@contextName", referenceData?.SourceFieldName);
            command.Parameters.AddWithValue("@key1", referenceData?.SourceDataValue1);
            command.Parameters.AddWithValue("@key2", referenceData?.SourceDataValue2 ?? string.Empty);

            connection.Open();
            using var reader = command.ExecuteReader();
            var resultValue = string.Empty;
            while (reader.Read())
            {
                // Concatenate all the result set
                if (!string.IsNullOrEmpty(resultValue))
                    resultValue = resultValue + "," + reader.GetString(0);
                else
                    resultValue = reader.GetString(0);
            }
            return resultValue;
        }

        /// <summary>
        /// Reference Data Lookup Operation (Async)
        /// </summary>
        /// <param name="referenceData">Object containing reference data</param>
        /// <param name="sqlConnString">Database connection string</param>
        /// <returns>Target Data (list)</returns>
        public async Task<string> LookUpReferenceDataAsync(ReferenceData referenceData, string sqlConnString)
        {
            using var connection = new SqlConnection(sqlConnString);
            var command = new SqlCommand("[dbo].[LookUpReferenceData]", connection)
            {
                CommandType = System.Data.CommandType.StoredProcedure
            };
            command.Parameters.AddWithValue("@srcSysName", referenceData?.SourceSystem);
            command.Parameters.AddWithValue("@dstSysName", referenceData?.TargetSystem);
            command.Parameters.AddWithValue("@contextName", referenceData?.SourceFieldName);
            command.Parameters.AddWithValue("@key1", referenceData?.SourceDataValue1);
            command.Parameters.AddWithValue("@key2", referenceData?.SourceDataValue2 ?? string.Empty);

            connection.Open();
            using var reader = await command.ExecuteReaderAsync();
            var resultValue = string.Empty;
            while (reader.Read())
            {
                // Concatenate all the result set
                if (!string.IsNullOrEmpty(resultValue))
                    resultValue = resultValue + "," + reader.GetString(0);
                else
                    resultValue = reader.GetString(0);
            }
            return resultValue;
        }
    }
}
