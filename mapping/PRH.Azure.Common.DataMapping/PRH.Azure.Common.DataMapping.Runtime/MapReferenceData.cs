using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Attributes;
using Microsoft.Azure.WebJobs.Extensions.OpenApi.Core.Enums;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Microsoft.Extensions.Configuration.AzureAppConfiguration;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

using PRH.Azure.Common.DataMapping.Common.Models;
using PRH.Azure.Common.DataMapping.Redis.Interfaces;
using PRH.Azure.Common.DataMapping.SQL.Interfaces;
using Microsoft.Extensions.Options;

namespace PRH.Azure.Common.DataMapping.Runtime
{
    /// <summary>
    /// Map Reference Data using Azure Function
    /// </summary>
    public class MapReferenceData
    {
        private readonly IRedisCacheAccessor _redisCacheAccessor;
        private readonly IReferenceDataOperations _referenceDataOperations;
        private readonly ILogger<MapReferenceData> _logger;
        private readonly IOptionsSnapshot<Configuration> _configuration;
        private readonly IConfigurationRefresher _configurationRefresher;

        public MapReferenceData(IRedisCacheAccessor redisCacheAccessor, IReferenceDataOperations referenceDataOperations,
            ILogger<MapReferenceData> logger, IOptionsSnapshot<Configuration> configuration, IConfigurationRefresherProvider refresherProvider)
        {
            _redisCacheAccessor = redisCacheAccessor;
            _referenceDataOperations = referenceDataOperations;
            _logger = logger;
            _configuration = configuration;
            _configurationRefresher = refresherProvider?.Refreshers?.First();
        }

        /// <summary>
        /// Function to accomplish data mapping by using either cached or database
        /// values (target data & target meta data) of reference data object
        /// </summary>
        /// <param name="req"><see cref="HttpRequest"/>containing list of reference data</param>
        /// <returns>list of reference data containing cache/database values</returns>
        [FunctionName("MapReferenceData")]
        [OpenApiOperation(operationId: "Run", tags: new[] { "name" })]
        [OpenApiSecurity("function_key", SecuritySchemeType.ApiKey, Name = "code", In = OpenApiSecurityLocationType.Query)]
        [OpenApiParameter(name: "name", In = ParameterLocation.Query, Required = true, Type = typeof(string), Description = "The **Name** parameter")]
        [OpenApiResponseWithBody(statusCode: HttpStatusCode.OK, contentType: "text/plain", bodyType: typeof(string), Description = "The OK response")]
        public async Task<IActionResult> RunAsync(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = "bag")] HttpRequest req)
        {
            // Set response content type to application/json
            req.HttpContext.Response.Headers.Add("Content-Type", "application/json");

            // Refresh the configuration
            if (_configurationRefresher is not null)
            {
                await _configurationRefresher.TryRefreshAsync();
            }

            // Read the configuration
            var cacheConnectionString = _configuration?.Value?.RedisCacheConnectionString;
            var dbConnectionString = _configuration?.Value?.DbConnectionString;
            var daysToCache = _configuration?.Value?.DaysToCache;

            // TO-DO - something to get from the request - logic app workflow to send???
            // Get file name that's being processed
            var fileName = $"edition_report{DateTime.Today:yyyyMMdd}.xml";

            // Process the incoming request and load it as reference data object
            List<ReferenceData> data = null;
            List<ReferenceData> receivedData = null;
            try
            {
                string requestBody = new StreamReader(req?.Body).ReadToEnd();
                _logger.LogInformation($"[{fileName}] Received Map Request: {requestBody}");
                data = JsonConvert.DeserializeObject<List<ReferenceData>>(requestBody);
                receivedData = data;
            }
            catch(Exception ex)
            {
                _logger.LogError($"[{fileName}] Exception occurred while trying to create reference data object out of the received request. Exception: {ex.Message}");
            }

            // Return BadRequest response if the request is not processed successfully
            if (data is null)
            {
                return new BadRequestObjectResult("Invalid request or no requst body found");
            }

            // Loop through each item in referenced data list
            foreach (var item in data)
            {
                // Generate cache key
                var cacheKey = $"{item?.SourceSystem}-{item?.TargetSystem}-{item?.SourceFieldName}-{item?.SourceDataValue1}-{item?.SourceDataValue2}";

                // Check for target data in cache
                string retValue = string.Empty;
                try
                {
                    retValue = await _redisCacheAccessor.GetCacheAsync(cacheConnectionString, cacheKey);
                }
                catch (Exception ex)
                {
                    _logger.LogError($"[{fileName}][{cacheKey}] Exception occurred while trying to retrieve the cached data from Redis. Exception: {ex.Message}");
                }

                // If not cached
                if (string.IsNullOrWhiteSpace(retValue))
                {
                    _logger.LogInformation($"[{fileName}][{cacheKey}] Matching data not available in the cache. Fetching it from the database...");
                    var cacheValue = string.Empty;
                    try
                    {
                        // Fetch it from database
                        cacheValue = await _referenceDataOperations.LookUpReferenceDataAsync(item, dbConnectionString);
                        retValue = cacheValue;
                    }
                    catch (Exception ex)
                    {
                        // Log
                        _logger.LogError($"[{fileName}][{cacheKey}] Exception occurred while trying to retrieve target data from the database. Exception: {ex.Message}");
                    }
                    // If database returns a valid target data
                    if (!string.IsNullOrWhiteSpace(cacheValue))
                    {
                        try
                        {
                            // Store it in redis cache
                            await _redisCacheAccessor.SetCacheAsync(cacheConnectionString, cacheKey, cacheValue, daysToCache);
                            _logger.LogInformation($"[{fileName}][{cacheKey}] Retrieved data \"{cacheValue}\" from the database has been stored in the cache");
                        }
                        catch (Exception ex)
                        {
                            // Log
                            _logger.LogError($"[{fileName}][{cacheKey}] Exception occurred while trying to store data in Redis cache. Exception: {ex.Message}");
                        }
                    }
                    else
                    {
                        // Log
                        _logger.LogInformation($"[{fileName}][{cacheKey}] Matching data not available in the database either.");
                    }
                }
                else
                {
                    // Log
                    _logger.LogInformation($"[{fileName}][{cacheKey}] Matching data \"{retValue}\" retrieved from the cache");
                }
                // Inject cached or database target data
                item.TargetData = retValue;
            }

            // Serialize initially received reference data as default response message
            string responseBody = JsonConvert.SerializeObject(receivedData);
            try
            {
                // Serialize mapped reference data
                responseBody = JsonConvert.SerializeObject(data);
                _logger.LogInformation($"[{fileName}] Mapped Response: {responseBody}");
            }
            catch(Exception ex)
            {
                // Log
                _logger.LogError($"[{fileName}] Failed to serialize the data back into JSON response message.  Exception: {ex.Message}");
            }

            // Return back the serialized reference data
            return new OkObjectResult(responseBody);
        }
    }
}
