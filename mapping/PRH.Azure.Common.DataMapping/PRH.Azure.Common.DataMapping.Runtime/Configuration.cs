﻿namespace PRH.Azure.Common.DataMapping.Runtime
{
    public class Configuration
    {
        public string RedisCacheConnectionString { get; set; }
        public string DbConnectionString { get; set; }
        public int DaysToCache { get; set; }
    }
}
