using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

using PRH.Azure.Common.DataMapping.Redis;
using PRH.Azure.Common.DataMapping.Redis.Interfaces;
using PRH.Azure.Common.DataMapping.SQL;
using PRH.Azure.Common.DataMapping.SQL.Interfaces;
using Azure.Identity;

[assembly: FunctionsStartup(typeof(PRH.Azure.Common.DataMapping.Runtime.Startup))]
namespace PRH.Azure.Common.DataMapping.Runtime
{
    /// <summary>
    /// Function Startup
    /// </summary>
    public class Startup: FunctionsStartup
    {
        /// <summary>
        /// Loads configurations related to this runtime from application configuration using managed identity
        /// </summary>
        /// <param name="builder"></param>
        public override void ConfigureAppConfiguration(IFunctionsConfigurationBuilder builder)
        {
            builder.ConfigurationBuilder.AddAzureAppConfiguration(options =>
            {
                options.Connect(new Uri(Environment.GetEnvironmentVariable("appConfigEndpoint")), new DefaultAzureCredential())
                    .Select("PRH.Azure.Common.DataMapping:*")
                    .ConfigureRefresh(refreshOptions =>
                        refreshOptions.Register("PRH.Azure.Common.DataMapping:refreshAll", refreshAll: true));
            });
        }

        /// <summary>
        /// Adds services required for this function runtime
        /// </summary>
        /// <param name="builder"></param>
        public override void Configure(IFunctionsHostBuilder builder)
        {
            builder.Services.AddSingleton<IRedisCacheAccessor, RedisCacheAccessor>();
            builder.Services.AddSingleton<ICacheConnector, CacheConnector>();
            builder.Services.AddSingleton<IReferenceDataOperations, ReferenceDataOperations>();
            builder.Services.AddLogging();

            builder.Services.AddAzureAppConfiguration();

            var configuration = builder.GetContext().Configuration;
            builder.Services.Configure<Configuration>(configuration.GetSection("PRH.Azure.Common.DataMapping"));
        }
    }
}
