﻿using Microsoft.Extensions.Logging;
using Moq;
using PRH.Azure.Common.DataMapping.Redis.Interfaces;
using PRH.Azure.Common.DataMapping.SQL.Interfaces;

namespace PRH.Azure.Common.DataMapping.Runtime.Tests
{
    public class TestFixture: IDisposable
    {
        private readonly Mock<IRedisCacheAccessor> _redisCacheAccessor;
        private readonly Mock<IReferenceDataOperations> _referenceDataOperations;
        private readonly Mock<ILogger<MapReferenceData>> _logger;

        public MapReferenceData mapReferenceData;

        public TestFixture() {
            _redisCacheAccessor = new Mock<IRedisCacheAccessor>();
            _referenceDataOperations = new Mock<IReferenceDataOperations>();
            _logger = new Mock<ILogger<MapReferenceData>>();
            mapReferenceData = new MapReferenceData(_redisCacheAccessor.Object, _referenceDataOperations.Object, _logger.Object);
        }

        public void Dispose()
        {
        }
    }
}
