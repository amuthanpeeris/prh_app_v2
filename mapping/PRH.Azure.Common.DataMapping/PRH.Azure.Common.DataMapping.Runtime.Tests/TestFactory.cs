﻿using Microsoft.AspNetCore.Http;
using System.Text;

namespace PRH.Azure.Common.DataMapping.Runtime.Tests
{
    /// <summary>
    /// Helper class to FunctionsTests
    /// </summary>
    public static class TestFactory
    {
        /// <summary>
        /// Creates a Http Request out of a json string
        /// </summary>
        /// <param name="requestBody">json string</param>
        /// <returns>http request</returns>
        public static HttpRequest CreateHttpRequest(string requestBody)
        {
            var context = new DefaultHttpContext();
            var request = context.Request;
            byte[] byteArray = Encoding.ASCII.GetBytes(requestBody);
            MemoryStream stream = new(byteArray);
            request.Body = stream;
            return request;
        }
    }
}
