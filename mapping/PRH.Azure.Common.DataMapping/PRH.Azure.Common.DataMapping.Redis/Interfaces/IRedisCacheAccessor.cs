using StackExchange.Redis;
using System.Threading.Tasks;

namespace PRH.Azure.Common.DataMapping.Redis.Interfaces
{
    /// <summary>
    /// Interface of Redis Cache Accessor
    /// </summary>
    public interface IRedisCacheAccessor
    {
        void SetCache(string cacheConnectionString, string cacheKey, string cacheValue, int? daysToLive);
        Task SetCacheAsync(string cacheConnectionString, string cacheKey, string cacheValue, int? daysToLive);
        string GetCache(string cacheConnectionString, string cacheKey);
        Task<string> GetCacheAsync(string cacheConnectionString, string cacheKey);
        Task FlushDatabaseAsync(string cacheConnectionString);
    }
}
