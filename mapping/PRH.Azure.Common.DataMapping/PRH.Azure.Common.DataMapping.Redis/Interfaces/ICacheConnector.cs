using StackExchange.Redis;
using System.Threading.Tasks;

namespace PRH.Azure.Common.DataMapping.Redis.Interfaces
{
    /// <summary>
    /// Interface to connect to Redis Server
    /// </summary>
    public interface ICacheConnector
    {
        IDatabase GetDbCache(string cacheConnectionString);
        Task<IDatabase> GetDbCacheAsync(string cacheConnectionString);
        void FlushDatabase(string cacheConnectionString);
        Task FlushDatabaseAsync(string cacheConnectionString);
    }
}
