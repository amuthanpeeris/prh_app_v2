using StackExchange.Redis;
using System.Collections.Generic;
using System.Threading.Tasks;

using PRH.Azure.Common.DataMapping.Redis.Interfaces;
using System.Net;

namespace PRH.Azure.Common.DataMapping.Redis
{
    /// <summary>
    /// Redis Connector
    /// </summary>
    public class CacheConnector: ICacheConnector
    {
        private static Dictionary<string, ConnectionMultiplexer> redisConnections;

        /// <summary>
        /// Connect to Redis Database
        /// </summary>
        /// <param name="cacheConnectionString"></param>
        /// <returns>an interative connection to redis database</returns>
        public IDatabase GetDbCache(string cacheConnectionString)
        {
            redisConnections ??= new Dictionary<string, ConnectionMultiplexer>();
            if (!redisConnections.TryGetValue(cacheConnectionString, out ConnectionMultiplexer connection))
            {
                connection = ConnectionMultiplexer.Connect(cacheConnectionString);
                redisConnections.Add(cacheConnectionString, connection);
            }
            return connection.GetDatabase();
        }

        /// <summary>
        /// Connect to Redis Database
        /// </summary>
        /// <param name="cacheConnectionString"></param>
        /// <returns>an interative connection to redis database</returns>
        public async Task<IDatabase> GetDbCacheAsync(string cacheConnectionString)
        {
            redisConnections ??= new Dictionary<string, ConnectionMultiplexer>();
            if (!redisConnections.TryGetValue(cacheConnectionString, out ConnectionMultiplexer connection))
            {
                connection = await ConnectionMultiplexer.ConnectAsync(cacheConnectionString);
                redisConnections.Add(cacheConnectionString, connection);
            }
            return connection.GetDatabase();
        }

        /// <summary>
        /// Flushes Redis Database
        /// </summary>
        /// <param name="cacheConnectionString"></param>
        public void FlushDatabase(string cacheConnectionString)
        {
            redisConnections ??= new Dictionary<string, ConnectionMultiplexer>();
            if (!redisConnections.TryGetValue(cacheConnectionString, out ConnectionMultiplexer connection))
            {
                connection = ConnectionMultiplexer.Connect(cacheConnectionString);
                redisConnections.Add(cacheConnectionString, connection);
            }
            EndPoint[] endPoints = connection.GetEndPoints();
            foreach (var endPoint in endPoints)
            {
                connection.GetServer(endPoint).FlushDatabase();
            }
        }

        /// <summary>
        /// Flushes Redis Database
        /// </summary>
        /// <param name="cacheConnectionString"></param>
        public async Task FlushDatabaseAsync(string cacheConnectionString)
        {
            redisConnections ??= new Dictionary<string, ConnectionMultiplexer>();
            if (!redisConnections.TryGetValue(cacheConnectionString, out ConnectionMultiplexer connection))
            {
                connection = await ConnectionMultiplexer.ConnectAsync(cacheConnectionString);
                redisConnections.Add(cacheConnectionString, connection);
            }
            EndPoint[] endPoints = connection.GetEndPoints();
            foreach (var endPoint in endPoints)
            {
                connection.GetServer(endPoint).FlushDatabase();
            }
        }
    }
}
