using StackExchange.Redis;
using System.Threading.Tasks;

using PRH.Azure.Common.DataMapping.Redis.Interfaces;

namespace PRH.Azure.Common.DataMapping.Redis
{
    public class RedisCacheAccessor: IRedisCacheAccessor
    {
        private readonly ICacheConnector _cacheConnector;

        public RedisCacheAccessor(ICacheConnector cacheConnector)
        {
            _cacheConnector = cacheConnector;
        }

        /// <summary>
        /// Store key/value pair in redis cache
        /// </summary>
        /// <param name="cacheConnectionString">Redis connection string</param>
        /// <param name="cacheKey">Key to store</param>
        /// <param name="cacheValue">Value to store</param>
        /// <param name="daysToLive">Number of days the key will be active in cache</param>
        public void SetCache(string cacheConnectionString, string cacheKey, string cacheValue, int? daysToLive)
        {
            IDatabase dbCache = _cacheConnector.GetDbCache(cacheConnectionString);
            dbCache.StringSet(cacheKey, cacheValue, new System.TimeSpan((daysToLive ?? 1) * 24, 0,0));
        }

        /// <summary>
        /// Store key/value pair in redis cache
        /// </summary>
        /// <param name="cacheConnectionString">Redis connection string</param>
        /// <param name="cacheKey">Key to store</param>
        /// <param name="cacheValue">Value to store</param>
        /// <param name="daysToLive">Number of days the key will be active in cache</param>
        public async Task SetCacheAsync(string cacheConnectionString, string cacheKey, string cacheValue, int? daysToLive)
        {
            IDatabase dbCache = await _cacheConnector.GetDbCacheAsync(cacheConnectionString);
            await dbCache.StringSetAsync(cacheKey, cacheValue, new System.TimeSpan((daysToLive ?? 1) * 24, 0, 0));
        }

        /// <summary>
        /// Retrieve the cached data in redis
        /// </summary>
        /// <param name="cacheConnectionString">Redis connection string</param>
        /// <param name="cacheKey">Key to retrieve cache value</param>
        /// <returns></returns>
        public string GetCache(string cacheConnectionString, string cacheKey)
        {
            IDatabase dbCache = _cacheConnector.GetDbCache(cacheConnectionString);
            return (string) dbCache.StringGet(cacheKey);
        }

        /// <summary>
        /// Retrieve the cached data in redis
        /// </summary>
        /// <param name="cacheConnectionString">Redis connection string</param>
        /// <param name="cacheKey">Key to retrieve cache value</param>
        /// <returns></returns>
        public async Task<string> GetCacheAsync(string cacheConnectionString, string cacheKey)
        {
            IDatabase dbCache = await _cacheConnector.GetDbCacheAsync(cacheConnectionString);
            return (string) await dbCache.StringGetAsync(cacheKey);
        }

        public async Task FlushDatabaseAsync(string cacheConnectionString)
        {
            await _cacheConnector.FlushDatabaseAsync(cacheConnectionString);
        }
    }
}
