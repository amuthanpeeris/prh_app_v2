namespace PRH.Azure.Common.DataMapping.Common.Models
{
    /// <summary>
    /// Model representing Reference Data
    /// </summary>
    public class ReferenceData
    {
        public string SourceSystem { get; set; }
        public string TargetSystem { get; set; }
        public string SourceFieldName { get; set; }
        public string SourceDataValue1 { get; set; }
        public string SourceDataValue2 { get; set; }
        //public string SourceMetaData { get; set; }
        public string TargetData { get; set; }
        //public string TargetMetaData { get; set; }
    }
}
