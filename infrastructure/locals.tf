locals {
  environment = terraform.workspace

  keyvault_ref         = "kv-${var.role}"
  sql_ref              = "sql-${var.role}"
  datamap_ref          = "datamap-${var.role}"
  rulesmap_ref         = "rulesmap-${var.role}"
  logicapp_ref         = "${var.titlefeed_ref}-${var.role}"
  pubeasy_logic_ref    = "${var.pubeasy_ref}-${var.role}"
  pubeasyfn_ref        = "pubeasyfn-${var.role}"
  ussap_logic_ref      = "${var.ussap_ref}-${var.role}"
  ussap_func_ref       = "${var.ussap_ref}-fn-${var.role}"
  ussaptitle_logic_ref = "${var.ussap_ref}-title-${var.role}"
  ussaptitle_func_ref  = "${var.ussap_ref}-title-fn-${var.role}"
  ussappipo_logic_ref  = "${var.ussap_ref}-pipo-${var.role}"
  ussappipo_func_ref   = "${var.ussap_ref}-pipo-fn-${var.role}"

  common_tags = {
    Environment       = terraform.workspace
    Team              = var.Team
    BusinessUnit      = var.BusinessUnit
    BusinessUnitOwner = var.BusinessUnitOwner
    ProjectCode       = var.ProjectCode
    CostCentre        = var.CostCentre
  }

  anz_tags = {
    SourceSystem       = "PRH Biblio"
    TargetSystem       = "ANZ Biblio"
    LegacyId           = "OP-004"
    SNOWId             = "ADI-ANZ-00004-A"
    DPIAClassification = "Basic Personal Data (Authors name), Book Data"
    External           = "Yes"
  }

  pubeasy_tags = {
    SourceSystem       = "PRH Biblio"
    TargetSystem       = "PRH PubEasy"
    SNOWId             = "ADI-UK-00190-A"
    DPIAClassification = "Basic Personal Data (Authors name), Book Data"
    External           = "Yes"
  }

  ussap_tags = {
    SourceSystem       = "PRH Biblio"
    TargetSystem       = "US SAP"
    DPIAClassification = "Basic Personal Data (Authors name), Book Data"
    External           = "Yes"
  }

  ussaptitle_tags = {
    SourceSystem       = "PRH Biblio"
    TargetSystem       = "US SAP"
    DPIAClassification = "Basic Personal Data (Authors name), Book Data"
    External           = "Yes"
  }

  ussappipo_tags = {
    SourceSystem       = "PRH Biblio"
    TargetSystem       = "US SAP"
    DPIAClassification = "Basic Personal Data (Authors name), Book Data"
    External           = "Yes"
  }
}
