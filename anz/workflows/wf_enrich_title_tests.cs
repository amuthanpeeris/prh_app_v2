using IPB.LogicApp.Standard.Testing.Local;
using IPB.LogicApp.Standard.Testing.Local.Host;
using IPB.LogicApp.Standard.Testing.Model;
using IPB.LogicApp.Standard.Testing.Model.WorkflowRunActionDetails;
using IPB.LogicApp.Standard.Testing.Model.WorkflowRunOverview;
using System.Text;
using Xunit;
using FluentAssertions;
using System.Net;

namespace prh.azure.interfaces.titlefeed.workflows.tests
{
    /// <summary>
    /// Test wf-enrich-title workflow using IPB.LogicApp.Standard.Testing.Local framework
    /// This requires emulator/azurite running
    /// </summary>
    public class wf_enrich_title_tests
    {
        /// <summary>
        /// Test to see if workflow runs successfully when a valid request is passed in
        /// </summary>
        [Fact]
        public void HttpTrigger_Should_Return_Accepted_Status()
        {
            // Assign
            var pathToFiles = @"..\..\..\workflows";
            var workflowToTestName = "wf-enrich-title";
            var workflowTestHostBuilder = new WorkflowTestHostBuilder(pathToFiles);
            workflowTestHostBuilder.Workflows.Add(workflowToTestName);
            var logicAppTestManager = new LogicAppTestManager(new LogicAppTestManagerArgs
            {
                WorkflowName = workflowToTestName
            });
            WorkFlowResponse response;
            var request = "<output><record><work_cover_title>Elixir</work_cover_title><work_id>442544</work_id><imprint_publisher>" +
                "</imprint_publisher><edition_binding></edition_binding></record></output>";

            // Act
            logicAppTestManager.Setup();
            using (var workflowTestHost = workflowTestHostBuilder.LoadAndBuild())
            {
                var content = new StringContent(request, Encoding.UTF8, "application/xml");
                response = logicAppTestManager.TriggerLogicAppWithPost(content);
            }

            // Assert
            response.HttpResponse.StatusCode.Should().Be(HttpStatusCode.Accepted);
        }

        /// <summary>
        /// Test to see if workflow returns bad request when empty request is passed in
        /// </summary>
        [Fact]
        public void HttpTrigger_Should_Return_BadRequest_Status()
        {
            // Assign
            var pathToFiles = @"..\..\..\workflows";
            var workflowToTestName = "wf-enrich-title";
            var workflowTestHostBuilder = new WorkflowTestHostBuilder(pathToFiles);
            workflowTestHostBuilder.Workflows.Add(workflowToTestName);
            var logicAppTestManager = new LogicAppTestManager(new LogicAppTestManagerArgs
            {
                WorkflowName = workflowToTestName
            });
            WorkFlowResponse response;
            var request = "";

            // Act
            logicAppTestManager.Setup();
            using (var workflowTestHost = workflowTestHostBuilder.LoadAndBuild())
            {
                var content = new StringContent(request, Encoding.UTF8, "application/xml");
                response = logicAppTestManager.TriggerLogicAppWithPost(content);
            }

            // Assert
            response.HttpResponse.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
    }
}
