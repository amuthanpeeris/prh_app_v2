﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Primitives;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PRH.Azure.Common.Rules.Runtime;
using static NRules.RuleModel.Builders.RuleTransformation;


namespace PRH.Azure.Common.Rules.Tests
{
    public class SimpleMapTest: FunctionTest
    {
        [Fact]
        public void MapsCorrectly()
        {
            // Arrange
            var requestMessage = @"
            {
                'Source':
                {
                    'Id':'124443',
                    'InputValue':'Test'
                },
                'Target':
                {
                    'OutputValue':''
                },
                'MapName': 'SampleMaps.SimpleMap'
            }"
            ;

            var logger = new Mock<ILogger<MapperRuleHost>>();
            var config = new Mock<IConfiguration>();

            var host = new MapperRuleHost(config.Object, logger.Object);
            // Act
            var map = host.Execute(requestMessage,
                Directory.GetCurrentDirectory(),
                true);

            // Assert
            JToken? target;
            map.Target.TryGetValue("OutputValue", out target);

            Assert.Equal(target,"124443-Test");
        }
    }
}
