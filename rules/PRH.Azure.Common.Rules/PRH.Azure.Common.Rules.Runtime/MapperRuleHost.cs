﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using PRH.Azure.Common.Rules.Plugins;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using NRules.Fluent;
using System.IO;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NRules;
using PRH.Azure.Common.Rules.Runtime;

namespace PRH.Azure.Common.Rules.Runtime
{
    public interface IMapperRuleHost
    {
        MapRequest Execute(string requestBody, string pluginFolder, bool ignoreNullValues = false, string pluginSearchPath = "*.dll");
    }

    public class MapperRuleHost : IMapperRuleHost
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<MapperRuleHost> _logger;

        public MapperRuleHost(IConfiguration configuration, ILogger<MapperRuleHost> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        [ImportMany(typeof(IMapPlugin))] private IEnumerable<Lazy<IMapPlugin>> Plugins { get; set; }

        public MapRequest Execute(string requestBody, string pluginFolder, bool ignoreNullValues = false, string pluginSearchPath = "*.dll")
        {

            MapRequest map = JsonConvert.DeserializeObject<MapRequest>(requestBody);

            _logger.LogInformation($"MapperRulesHost : Plugin folder {pluginFolder}");

            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new DirectoryCatalog(pluginFolder, pluginSearchPath));

            var container = new CompositionContainer(catalog);
            container.ComposeParts(this);

            var messageTypes = GetPrimaryPlugin(map.MapName);

            var sourceMsg = Activator.CreateInstance(GetSourceMessageType(map.MapName));
            var targetMsg = Activator.CreateInstance(GetTargetMessageType(map.MapName));

            JsonConvert.PopulateObject(map.Source.ToString(), sourceMsg);
            JsonConvert.PopulateObject(map.Target.ToString(), targetMsg);

            var context = buildContext(messageTypes.Value.ConfigTokens);

            RuleRepository ruleRepository = new RuleRepository
            {
                Activator = new RuleActivator(context)
            };

            foreach (var pi in GetMapPlugins(map.MapName))
                ruleRepository.Load(x => x.From(pi.Value.GetType().Assembly));

            var factory = ruleRepository.Compile();

            var session = factory.CreateSession();

            session.Insert(sourceMsg);
            session.Insert(targetMsg);

            session.Fire();

            JsonSerializer serializer = new JsonSerializer();

            if(ignoreNullValues){
                serializer.NullValueHandling = NullValueHandling.Ignore;
            }

            map.Target = JObject.FromObject(targetMsg, serializer);

            return map;
        }

        private Context buildContext(IEnumerable<string> configTokens)
        {
            Context ctx = new Context
            {
                Tokens = new Dictionary<string, string>(),
                Configuration = _configuration
            };

            foreach (var configToken in configTokens)
            {
                var value = _configuration[configToken];
                ctx.Tokens.Add(configToken, value);
            }

            return ctx;
        }

        private IEnumerable<Lazy<IMapPlugin>> GetMapPlugins(string mapName)
        {
            return Plugins
                .Where(pi => pi.Value.MapName == mapName)
                .GroupBy(pi => pi.Value.GetType().Assembly)
                .Select(grp => grp.FirstOrDefault());
        }

        private Lazy<IMapPlugin> GetPrimaryPlugin(string mapName)
        {
            var primaryPlugin = GetMapPlugins(mapName).FirstOrDefault();

            if (primaryPlugin == null)
                throw new ApplicationException($"No plugins found to process map [{mapName}]");

            return primaryPlugin;
        }

        private Type GetSourceMessageType(string mapName)
        {
            return GetPrimaryPlugin(mapName).Value.SourceMessageType;
        }

        private Type GetTargetMessageType(string mapName)
        {
            return GetPrimaryPlugin(mapName).Value.TargetMessageType;
        }
    }
}
