using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json;
using static NRules.RuleModel.Builders.RuleTransformation;

namespace PRH.Azure.Common.Rules.Runtime
{
    public class ExecuteMapRules
    {
        private readonly ILogger<ExecuteMapRules> _logger;
        private readonly IConfiguration _configuration;
        private readonly IMapperRuleHost _mapperRuleHost;

        public ExecuteMapRules(ILogger<ExecuteMapRules> log, IConfiguration configuration, IMapperRuleHost mapperRuleHost)
        {
            _logger = log;
            _configuration = configuration;
            _mapperRuleHost = mapperRuleHost;
        }

        [Function("ExecuteMapRules")]

        public async Task<HttpResponseData> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = "execute")] HttpRequestData req, FunctionContext context)
        {
            try
            {

                var requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                var ignoreNulls = IgnoreNulls(req);
                var pluginFolder = Path.Combine(Path.GetDirectoryName(context.FunctionDefinition.PathToAssembly)!, Environment.GetEnvironmentVariable("PluginFolder"));

                var map = _mapperRuleHost.Execute(requestBody, pluginFolder, ignoreNulls);

                return await BuildResponse(req, map);

                /*
                var response = req.CreateResponse(HttpStatusCode.OK);
                response.Headers.Add("Content-Type", "application/json");
                var jsonResponse = JsonConvert.SerializeObject(map);
                await response.WriteStringAsync(jsonResponse, Encoding.UTF8);
                return response;
                */
            }
            catch (Exception ex)
            {
               //return new BadRequestObjectResult(ex);
                throw;
            }
        }

        private async Task<HttpResponseData> BuildResponse(HttpRequestData request, MapRequest map)
        {
            var response = request.CreateResponse(HttpStatusCode.OK);
            response.Headers.Add("Content-Type", "application/json");

            var jsonResponse = JsonConvert.SerializeObject(map);
            await response.WriteStringAsync(jsonResponse, Encoding.UTF8);

            return response;
        }

        private bool IgnoreNulls(HttpRequestData request)
        {
            request.Headers.TryGetValues("Ignore-NullValues", out var ignoreNullValues);
            return ignoreNullValues != null && Convert.ToBoolean(ignoreNullValues.First());
        }
    }
}
