using Newtonsoft.Json.Linq;

namespace PRH.Azure.Common.Rules.Runtime;

public class MapRequest
{
    public JObject Source { get; set; }
    public JObject Target { get; set; }
    public string MapName { get; set; }
}
