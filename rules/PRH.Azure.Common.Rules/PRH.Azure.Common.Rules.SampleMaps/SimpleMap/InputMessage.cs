﻿namespace PRH.Azure.Common.Rules.SampleMaps.SimpleMap;

public class InputMessage
{
    public int Id { get; set; }
    public string? InputValue { get; set; }
}
